﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using NimbusFox.KitsuneCore.V1.Classes;
using NimbusFox.KitsuneCore.V1.Classes.BlobRecord;
using Plukit.Base;

namespace NimbusFox.TaskList.Classes.Record {
    public class ListRecord : BaseRecord {
        /// <inheritdoc />
        public ListRecord(BlobDatabase database, Blob blob, Guid id) : base(database, blob, id) {

        }

        public List<Guid> ItemRecords {
            get {
                var items = new List<Guid>();

                foreach (var list in _blob.FetchList("records")) {
                    items.Add(Guid.Parse(list.GetString()));
                }

                return items;
            }
            private set {
                if (_blob.Contains("records")) {
                    _blob.Delete("records");
                }

                var list = _blob.FetchList("records");

                foreach (var item in value) {
                    var entry = new BlobEntry(_blob.KeyValueIteratable["records"], 0);
                    entry.SetString(item.ToString());
                    list.Add(entry);
                }

                Save();
            }
        }

        public Guid AddRecord(string text, bool isChecked) {
            var record = TaskListHook.Instance.Database.CreateRecord<ListItemRecord>();
            record.Text = text;
            record.IsChecked = isChecked;

            var records = ItemRecords;

            records.Add(record.ID);
            ItemRecords = records;

            return record.ID;
        }

        public void RemoveRecord(Guid id) {
            var records = ItemRecords;
            if (records.Contains(id)) {
                TaskListHook.Instance.Database.RemoveRecord(id);
                records.Remove(id);
                ItemRecords = records;
            }
        }
    }
}
