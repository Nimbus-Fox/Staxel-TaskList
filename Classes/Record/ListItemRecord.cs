﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using NimbusFox.KitsuneCore.V1.Classes;
using NimbusFox.KitsuneCore.V1.Classes.BlobRecord;
using Plukit.Base;

namespace NimbusFox.TaskList.Classes.Record {
    public class ListItemRecord : BaseRecord {
        /// <inheritdoc />
        protected ListItemRecord(BlobDatabase database, Blob blob, Guid id) : base(database, blob, id) { }

        public string Text {
            get => _blob.GetString(nameof(Text), "");
            set {
                _blob.SetString(nameof(Text), value);
                Save();
            }
        }

        public bool IsChecked {
            get => _blob.GetBool(nameof(IsChecked), false);
            set {
                _blob.SetBool(nameof(IsChecked), value);
                Save();
            }
        }
    }
}
